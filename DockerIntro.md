History about Docker:
•	Docker was released in 2013 by a hosting company called Dot Cloud (Company no longer exists)
•	It’s been 5 years since Docker was released and now it’s a revolution.
•	Huge shift in infrastructure [Mainframe to PC, Bare metal to Virtualization, Data Center to Cloud, Containers]


Overview of Docker:
•	Docker is an open source platform for developing, shipping, and running applications.
•	Docker is an open-source project that automates the deployment of applications inside software containers, by providing an additional layer of abstraction and automation of operating system-level virtualization on Linux
•	Docker packages an application and all its dependencies in a ‘virtual container’ so that it can be run on any Linux system or distribution
•	Docker enables you to separate your applications from your infrastructure so you can deliver software quickly.
•	Docker provides the ability to package and run an application in a loosely isolated environment called a container
